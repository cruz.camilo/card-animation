package com.example.simulatortop


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_activity_card_flip.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val handler = Handler()
    private var progressBarValue = 0
    private var mShowingBack = false
    //Expresión regular
    val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity_card_flip);

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, CardFrontFragment.newInstance())
                .commit()
        }

        container.setOnClickListener {
//            flipCard()
        }

    }

    override fun onResume() {
        super.onResume()
        button_validate?.setOnClickListener{
            val emailValid = isEmailValid(email_input.text.toString())
            Toast.makeText(this, emailValid.toString(), Toast.LENGTH_LONG).show()
        }
    }

    // Método donde se usa la expresión regular
    fun isEmailValid(email: String): Boolean {
        return EMAIL_REGEX.toRegex().matches(email)
    }

    private fun flipCard() {
        if (mShowingBack) {
            supportFragmentManager.popBackStack()
            mShowingBack = false
            return
        }
        // Flip to the back.
        mShowingBack = true
        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the card, uses custom animations, and is part of the fragment manager's back stack.
        supportFragmentManager
            .beginTransaction()
            // Replace the default fragment animations with animator resources representing
            // rotations when switching to the back of the card, as well as animator
            // resources representing rotations when flipping back to the front (e.g. when
            // the system Back button is pressed).
            .setCustomAnimations(
                R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                R.animator.card_flip_left_in, R.animator.card_flip_left_out
            ) // Replace any fragments currently in the container view with a fragment
            // representing the next page (indicated by the just-incremented currentPage
            // variable).
            .replace(
                R.id.container, CardBackFragment.newInstance(), "CardBack") // Add this transaction to the back stack, allowing users to press Back
// to get to the front of the card.
            .addToBackStack(null) // Commit the transaction.
            .commit()
    }
}

class CardFrontFragment : androidx.fragment.app.Fragment() {

    companion object {
        fun newInstance(): CardFrontFragment {
            return CardFrontFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_main, container, false)
    }
}

class CardBackFragment : androidx.fragment.app.Fragment() {

    companion object {
        fun newInstance(): CardBackFragment {
            return CardBackFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_main_back, container, false)
    }
}